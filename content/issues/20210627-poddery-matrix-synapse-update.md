---
title: "Poddery Matrix Synapse Update"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2021-06-27 14:00:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2021-06-27 15:30:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
  - matrix
# - xmpp
# - diaspora
# - gitlab
# - website
# - mailing-lists
# - loomio
# - videos
# - planet
# - jitsi

# Don't change the value below
section: issue
---
Sunday 27/06/2021 14:00 IST is scheduled for updating synapse matrix server. The process may take upto 1 hour depending on the maintenance required and you may experience some downtime during this period.

Update: Synapse is on v0.36 now. This took a little more time than anticipated. But downtime was almost negligible as there were only a couple of restarts in between. 
