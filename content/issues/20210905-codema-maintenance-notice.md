---
title: "Codema Maintenance Notice"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2021-09-05 23:00:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2021-09-06 00:00:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
# - matrix
# - xmpp
# - diaspora
# - gitlab
# - website
# - mailing-lists
  - loomio
# - videos
# - planet
# - jitsi

# Don't change the value below
section: issue
---
Sunday 05/09/2021 22:00 IST is scheduled for updating loomio. The process may take up to 1 hour and you may experience some downtime for codema.in during this period.

Update: The update was cancelled due to an error in database migration. The service was restored to older state after about 1 hour of downtime.