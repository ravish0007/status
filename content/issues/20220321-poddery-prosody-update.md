---
title: "Prosody v0.11.13 update"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2022-03-21 21:45:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2022-03-21 22:14:00
# You can use: down, disrupted, notice
severity: disrupted
# affected sections (array). Uncomment the affected one's
affected:
 - xmpp 
  # Don't change the value below
section: issue
---
We updated Prosody to v11.13
