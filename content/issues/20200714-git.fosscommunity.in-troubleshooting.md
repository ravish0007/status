---
title: "git.fosscommunity.in: troubleshoot issues created by ruby update to 2.7"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2020-07-14 13:00:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2020-07-14 13:30:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
 - gitlab

# Don't change the value below
section: issue
---
All repos showed 503 error when viewing via web interface. It was caused by an
old version of bundler installed for ruby 2.5 not working with ruby 2.7. It was
fixed by removing the old version of bundler and restarting gitaly service.
