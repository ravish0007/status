---
title: "Poddery Matrix Synapse 1.44.0"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2021-10-06 22:30:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2021-10-06 22:49:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
  - matrix

# Don't change the value below
section: issue
---
We updated poddery's matrix-synapse to 1.44.0
