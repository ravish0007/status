---
title: "Poddery server restart"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2022-04-02 21:40:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2022-04-02 22:45:00
# You can use: down, disrupted, notice
severity: disrupted
# affected sections (array). Uncomment the affected one's
affected:
 - xmpp 
 - matrix
 - diaspora
# Don't change the value below
section: issue
---
We're have successfully restarted the server.
