---
title: "Jitsi Meet 2.0.5870 update"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2021-05-16 23:50:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2021-05-16 23:59:50
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
- jitsi

# Don't change the value below
section: issue
---
We have updated Jitsi Meet to v2.0.5870.
