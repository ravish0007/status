---
title: "Diaspora v0.7.15.0, Prosody v0.11.12-1, Matrix-Synapse v1.49.0-1 update "
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2022-01-15 22:35:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2022-01-16 02:05:00
# You can use: down, disrupted, notice
severity: disrupted
# affected sections (array). Uncomment the affected one's
affected:
 - matrix
 - xmpp
 - diaspora 
# Don't change the value below
section: issue
---
We have updated
- Diaspora to v0.7.15.0
- Prosody to v0.11.12, 
- Matrix-Synapse to v1.49.0-1
- System update. 
